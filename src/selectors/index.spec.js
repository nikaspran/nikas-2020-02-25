import { getVisibleTodos, getCompletedTodoCount } from './index'
import { SHOW_ALL, SHOW_COMPLETED, SHOW_ACTIVE } from '../constants/TodoFilters';

const { objectContaining } = expect;

describe('selectors', () => {
  describe('getVisibleTodos()', () => {
    const todos = [
      { id: 0, completed: true },
      { id: 1, completed: false },
    ];

    it('should show all todos if visibility filter is set to SHOW_ALL', () => {
      expect(getVisibleTodos({
        visibilityFilter: SHOW_ALL,
        todos,
      })).toEqual([
        objectContaining({ id: 0 }),
        objectContaining({ id: 1 }),
      ])
    })

    it('should only completed todos if visibility filter is set to SHOW_COMPLETED', () => {
      expect(getVisibleTodos({
        visibilityFilter: SHOW_COMPLETED,
        todos,
      })).toEqual([
        objectContaining({ id: 0 }),
      ])
    })

    it('should only active todos if visibility filter is set to SHOW_ACTIVE', () => {
      expect(getVisibleTodos({
        visibilityFilter: SHOW_ACTIVE,
        todos,
      })).toEqual([
        objectContaining({ id: 1 }),
      ])
    })

    it('should throw an error for an unknown filter', () => {
      expect(() => (
        getVisibleTodos({
          visibilityFilter: 'some filter',
          todos,
        })
      )).toThrow('Unknown filter: some filter')
    })
  })

  describe('getCompletedTodoCount()', () => {
    it('should return the number of completed todos', () => {
      expect(getCompletedTodoCount({
        todos: [
          { completed: true },
          { completed: false },
          { completed: true },
        ]
      })).toEqual(2)
    })
  })
})
