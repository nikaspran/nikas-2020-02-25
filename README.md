# nikas - 2020-02-25

## Installation

### Via Docker
* Prerequisites:
  * Docker
* `docker-compose up` - start the app in development mode and tests in watch mode
* `docker-compose up web` - start the app in development mode, accessible at http://localhost:3000/
* `docker-compose up test` - run tests in watch mode
* `docker-compose build` - rebuild the apps, including dependencies (`node_modules`)

### Via npm
* Prerequisites
  * Node 12.x - can use `nvm use` to automatically setup
* `npm install` - install required dependencies
* `npm start` - start the app in development mode, accessible at http://localhost:3000/
* `npm test` - start tests in watch mode
* `npm run lint` - run eslint

## Checklist
<!-- Double check you have completed the requirements. Feel free to add more. -->
- [x] Can run app within Docker without installing Node.js on host
- [x] Live-reload works in local environment
- [x] Review App for app
- [x] Review App for test code coverage report
- [x] ESLint
- [x] EditorConfig
- [x] Review README.md

## Demo
<!-- Recommend testing these pages with Chrome Incognito to ensure we can view -->
**Merge Request:** http://nikas-2020-02-25.s3-website-eu-west-1.amazonaws.com/feature-some-feature-branch/

**App Review App:** http://nikas-2020-02-25.s3-website-eu-west-1.amazonaws.com/master/

**Coverage Review App:** http://nikas-2020-02-25.s3-website-eu-west-1.amazonaws.com/master-coverage/

## Features
<!-- Any special features / interesting details with your implementation you want to explicitly note -->
* Review apps get automatically removed once merge requests are closed
* Added tests for visibilityFilter and selectors, plus did a tiny refactoring on the `getCompletedTodoCount()` selector

## Security

### Addressed:
* Ensure using latest package versions
* Set up automatic npm audit
* Bump Node to 12.x (latest LTS)
* Verify awscliv2 installer integrity and authenticity in the runner image
* Added a CSP meta tag

### TODO:
* Maybe clarify and separate production build dependencies (i.e. CRA required for production build, but test renderer not so much)
* Remove review app files after a certain period of time
* Auth for review apps (SSO/VPN/?)
* AWS: credential variables in Gitlab for production should be set to "Protected" or restricted to production environment only
* AWS: bucket security
  * Encrypt files & serve via cloudfront over HTTPS
  * Separate accounts for master/feature branches (i.e. production and staging)
* Docker images:
  * Make sure running as user, not root

## Improvements
<!-- What could be added? -->
* S3 buckets should be set up via Terraform or something like that
* Split cacheable and uncacheable uploads when deploying
* Optimise runner image to be leaner (via Alpine?)
  * Multi-stage build to trim down the awscli installer signature validation?
* Ensure docker-compose output retains colors
* Add husky - would need to either support both npm and docker-compose flows or decide in-org which we deem canonical
* Add a more rigid eslint configuration
* Set up prettier if desired
* Simplify .gitlab-ci.yml
  * need to investigate variable replacement method, had issues with transient variables (i.e. global `S3_BUCKET_FOLDER: $CI_COMMIT_REF_SLUG`)
* Wrap npm audit with a script so we can ignore mitigated advisories

---

## Other notes
<!-- Optional: Anything else you want to mention -->
* The `/ci-runner/Dockerfile` would probably be in a shared docker image repository, but added here for convenience
  * Could also be left in this repository if full encapsulation is desired, but would need to sort out individual CI for ci-runner images
* Netlify would possibly be a good alternative for S3 in this exercise, as it has an always-free tier (S3 free tier is 12 months only)
