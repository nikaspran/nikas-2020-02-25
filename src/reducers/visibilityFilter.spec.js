import visibilityFilter from './visibilityFilter'
import * as types from '../constants/ActionTypes'
import { SHOW_ALL, SHOW_ACTIVE } from '../constants/TodoFilters'

describe('visibilityFilter reducer', () => {
  it('should handle initial state', () => {
    expect(
      visibilityFilter(undefined, {})
    ).toEqual(SHOW_ALL)
  })

  it('should handle SET_VISIBILITY_FILTER', () => {
    expect(
      visibilityFilter(undefined, {
        type: types.SET_VISIBILITY_FILTER,
        filter: SHOW_ACTIVE
      })
    ).toEqual(SHOW_ACTIVE);
  })
})
